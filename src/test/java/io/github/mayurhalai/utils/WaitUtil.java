package io.github.mayurhalai.utils;

public class WaitUtil {
    public static void staticWait(int seconds) {
        staticWaitMilli(seconds * 1000);
    }

    public static void staticWaitMilli(int milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
