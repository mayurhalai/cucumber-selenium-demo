package io.github.mayurhalai.config;

import io.github.mayurhalai.spring.FrameworkConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Import(FrameworkConfiguration.class)
@ComponentScan("io.github.mayurhalai.e2e")
@PropertySource("classpath:test.properties")
public class TestConfiguration {
}
