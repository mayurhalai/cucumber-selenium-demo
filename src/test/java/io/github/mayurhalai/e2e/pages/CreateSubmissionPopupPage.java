package io.github.mayurhalai.e2e.pages;

import io.github.mayurhalai.annotation.PageObject;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static io.github.mayurhalai.global.TestConstants.WAIT_TIME;

@PageObject
public class CreateSubmissionPopupPage {
    @Autowired
    private WebDriver webDriver;

    @FindBy(xpath = "//div[@class='category-container col-lg-6']//div[@class='input-container']")
    private WebElement categorySelect;

    @FindBy(xpath = "//div[@class='category-container col-lg-6']//div[@class='select-body-container active']/div/div")
    private List<WebElement> categoryOptions;

    @FindBy(xpath = "//div[@class='forum-container col-lg-6']//div[@class='input-container']")
    private WebElement forumSelect;

    @FindBy(xpath = "//div[@class='forum-container col-lg-6']//div[@class='select-body-container active']/div/div")
    private List<WebElement> forumOptions;

    @FindBy(xpath = "//div[@class='pills-container']/input")
    private WebElement associateBillRecordInput;

    @FindBy(xpath = "//div[@class='typeahead-body-container active']/div")
    private List<WebElement> associateBillOptions;

    @FindBy(xpath = "//button[contains(.,'Create Submission')]")
    private WebElement createSubmissionButton;

    public void selectCategory(String category) {
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(categorySelect));
        categorySelect.click();
        for (WebElement element : categoryOptions) {
            if (category.equalsIgnoreCase(element.getText())) {
                new Actions(webDriver).moveToElement(element).click().perform();
            }
        }
    }

    public void selectForum(String forum) {
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(forumSelect));
        forumSelect.click();
        for (WebElement element : forumOptions) {
            if (forum.equalsIgnoreCase(element.getText())) {
                new Actions(webDriver).moveToElement(element).click().perform();
            }
        }
    }

    public void selectAssociateBill(String bill) {
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(associateBillRecordInput));
        associateBillRecordInput.sendKeys(bill.toLowerCase());
        for (WebElement element : associateBillOptions) {
            if (bill.equalsIgnoreCase(element.getText())) {
                new Actions(webDriver).moveToElement(element).click().perform();
            }
        }
    }

    public void submitForm() {
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(createSubmissionButton));
        createSubmissionButton.click();
    }
}
