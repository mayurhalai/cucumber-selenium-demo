package io.github.mayurhalai.e2e.pages;

import io.github.mayurhalai.annotation.PageObject;
import io.github.mayurhalai.utils.WaitUtil;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static io.github.mayurhalai.global.TestConstants.WAIT_TIME;

@PageObject
public class PrivateBillSubmissionPage {
    @Autowired
    private WebDriver webDriver;

    @FindBy(xpath = "//div[@class='proposed-container col-lg-6']//div[@class='input-container']")
    private WebElement proposedSelect;

    @FindBy(xpath = "//div[@class='proposed-container col-lg-6']//div[@class='select-body-container active']/div/div")
    private List<WebElement> proposedOptions;

    @FindBy(className = "cke_wysiwyg_div")
    private WebElement detailsInput;

    @FindBy(xpath = "//div[@class='col-lg-6']//div[@class='input-container']")
    private WebElement recipientSelect;

    @FindBy(xpath = "//div[@class='col-lg-6']//div[@class='select-body-container active']/div/div")
    private List<WebElement> recipientOptions;

    @FindBy(xpath = "//div[@class='saf']//button[@class='btn-lrg-primary']")
    private WebElement sendButton;

    @FindBy(className = "overlay")
    private WebElement overLay;

    public void selectProposedComment(String comment) {
        WebDriverWait wait = new WebDriverWait(webDriver, WAIT_TIME);
        wait.until(ExpectedConditions.visibilityOf(proposedSelect));
        wait.until(ExpectedConditions.invisibilityOf(overLay));
//        new Actions(webDriver).moveToElement(proposedSelect, 0, 80).click().perform();
        proposedSelect.click();
        for (WebElement element : proposedOptions) {
            if (comment.equalsIgnoreCase(element.getText())) {
                new Actions(webDriver).moveToElement(element).click().perform();
            }
        }
    }

    public void selectRecipient(String recipient) {
        ((JavascriptExecutor) webDriver).executeScript("window.scrollTo(0, document.body.scrollHeight)");
        new WebDriverWait(webDriver, WAIT_TIME).until(ExpectedConditions.visibilityOf(recipientSelect));
        recipientSelect.click();
        for (WebElement element : recipientOptions) {
            if (recipient.equalsIgnoreCase(element.getText())) {
                new Actions(webDriver).moveToElement(element).click().perform();
            }
        }
    }

    public void writeDetails(String details) {
        detailsInput.clear();
        detailsInput.sendKeys(details);
    }

    public void sendBill() {
        WaitUtil.staticWaitMilli(300);
        sendButton.click();
    }
}
