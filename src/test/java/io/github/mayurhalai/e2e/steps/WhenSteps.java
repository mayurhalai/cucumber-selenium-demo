package io.github.mayurhalai.e2e.steps;

import cucumber.api.java.en.When;
import io.github.mayurhalai.e2e.model.UserCredentials;
import io.github.mayurhalai.e2e.pages.CreateSubmissionPopupPage;
import io.github.mayurhalai.e2e.pages.HomePage;
import io.github.mayurhalai.e2e.pages.MicrosoftLoginPage;
import io.github.mayurhalai.e2e.pages.PrivateBillSubmissionPage;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

@Scope("test")
public class WhenSteps {
    @Autowired
    private WebDriver driver;
    @Autowired
    private MicrosoftLoginPage loginPage;
    @Autowired
    private UserCredentials credentials;
    @Autowired
    private HomePage homePage;
    @Autowired
    private CreateSubmissionPopupPage createSubmissionPopupPage;
    @Autowired
    private PrivateBillSubmissionPage privateBillSubmissionPage;

    @When("the user logs into the website")
    public void theUserLogsIntoTheWebsite() {
        driver.get("https://submissionssystemtest.azurewebsites.net ");
        loginPage.login(credentials);
    }

    @When("the user creates a submission")
    public void theUserCreatesASubmission() {
        homePage.isDisplayed();
        homePage.createSubmission();
        // Select category of submission
        createSubmissionPopupPage.selectCategory("Private Bill");
        createSubmissionPopupPage.selectForum("Seanad Éireann");
        createSubmissionPopupPage.selectAssociateBill("ABCD Test");
        createSubmissionPopupPage.submitForm();
        // fill up submission
        privateBillSubmissionPage.selectProposedComment("In Order");
        privateBillSubmissionPage.writeDetails("Some details of a bill.");
        privateBillSubmissionPage.selectRecipient("Anna Nolan");
        privateBillSubmissionPage.sendBill();
    }
}
