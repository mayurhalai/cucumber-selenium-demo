package io.github.mayurhalai.e2e.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:cucumber-features/CreateSubmission.feature",
        glue = {"io.github.mayurhalai.e2e.steps", "io.github.mayurhalai.cucumber"},
        plugin = {
                "pretty",
                "html:target/CreateSubmission",
                "json:target/CreateSubmission.json"
        })
public class CreateSubmissionTest {
}
