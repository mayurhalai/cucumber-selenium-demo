package io.github.mayurhalai.e2e.tests;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:cucumber-features/Authentication.feature",
        glue = {"io.github.mayurhalai.e2e.steps", "io.github.mayurhalai.cucumber"},
        plugin = {
                "pretty",
                "html:target/logintest",
                "json:target/logintest.json"
        })
public class LoginTest {
}
