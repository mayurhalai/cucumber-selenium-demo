package io.github.mayurhalai.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.CustomScopeConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
@ComponentScan("io.github.mayurhalai")
public class FrameworkConfiguration {

    @Bean
    @Autowired
    public CustomScopeConfigurer getThreadScope(TestScope testScope) {
        CustomScopeConfigurer cfg = new CustomScopeConfigurer();
        Map<String, Object> scopeMap = new HashMap<String, Object>();
        scopeMap.put("test", testScope);
        cfg.setScopes(scopeMap);
        return cfg;
    }
}
