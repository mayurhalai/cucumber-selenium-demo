package io.github.mayurhalai;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class WebDriverFactory {
    @Value("${chrome.driver.location:none}")
    private String chromeDriverLocation;

    @Value("${gecko.driver.location:none}")
    private String geckoDriverLocation;

    @Value("${browser:chrome}")
    private String browser;

    @Value("${selenium.grid.url:none}")
    private String seleniumGridUrl;

    @Bean
    @Scope("test")
    public WebDriver getDriver() {
        if (!seleniumGridUrl.equals("none")) {
            DesiredCapabilities capabilities;
            if (browser.equals("chrome")) {
                capabilities = DesiredCapabilities.chrome();
            } else {
                capabilities = DesiredCapabilities.firefox();
            }
            try {
                return new RemoteWebDriver(new URL(seleniumGridUrl), capabilities);
            } catch (MalformedURLException e) {
                throw new RuntimeException("Invalid selenium grid url.", e);
            }
        } else {
            if (browser.equals("chrome")) {
                System.setProperty("webdriver.chrome.driver", chromeDriverLocation);
                return new ChromeDriver();
            } else {
                System.setProperty("webdriver.firefox.marionette", geckoDriverLocation);
                return new FirefoxDriver();
            }
        }
    }
}
